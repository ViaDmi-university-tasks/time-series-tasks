#include <stdio.h>
#include <stdlib.h>
#include "series_model.h"

void go_next_line(FILE *file, int count)
{
    char tmp;
    for (int i = 0; i < count; i++)
    {
        do
            tmp = fgetc(file);
        while (tmp != '\n');
    }
}

void read_model(char *file_name, struct series_model *model)
{
    FILE *file;
    file = fopen(file_name, "r");
    go_next_line(file, 1);
    fscanf(file, "%lf %i", &model->t_step, &model->steps_count);
    go_next_line(file, 2);
    fscanf(file, "%lf %lf %lf", &model->amplitude, &model->frequency, &model->phase);
    go_next_line(file, 2);
    fscanf(file, "%lf %lf %lf", &model->SNR, &model->lin_trend_a, &model->lin_trend_b);
    fclose(file);
}

void print_series_in_file(char *file_name, int series_size, double *times, double *series)
{
    FILE *file;
    file = fopen(file_name, "w");
    fprintf(file, "# %i \n", series_size);
    for (int i = 0; i < series_size; i++)
        fprintf(file, "%.15e %.15e \n", times[i], series[i]);
    fclose(file);
}

int main(int argc, char *argv[])
{
    double *times, *series;
    struct series_model model;

    read_model("./input.dat", &model);
    get_series(&model, &times, &series);
    print_series_in_file("./data/series.dat", model.steps_count, times, series);

    return 0;
}