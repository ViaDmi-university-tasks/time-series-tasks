#include "scats.h"
#include "series_model.h"
#include <gsl/gsl_fit.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics_double.h>
#include <math.h>

void print_series_in_file(char *file_name, int series_size, double *series)
{
    FILE *file;
    file = fopen(file_name, "w");
    fprintf(file, "# %i \n", series_size);
    for (int i = 0; i < series_size; i++)
        fprintf(file, "%.15e \n", series[i]);
    fclose(file);
}

void print_series2_in_file(char *file_name, int series_size, double *series, double *series1)
{
    FILE *file;
    file = fopen(file_name, "w");
    fprintf(file, "# %i \n", series_size);
    for (int i = 0; i < series_size; i++)
        fprintf(file, "%.15e %.15e \n", series[i], series1[i]);
    fclose(file);
}

unsigned long working_seed = 123;

series_model get_random_model_1(int series_size)
{
    gsl_rng *r1, *r2, *r3, *r4;
    gsl_rng_env_setup();
    r1 = gsl_rng_alloc(gsl_rng_default);
    r2 = gsl_rng_alloc(gsl_rng_default);
    r3 = gsl_rng_alloc(gsl_rng_default);
    r4 = gsl_rng_alloc(gsl_rng_default);
    gsl_rng_set(r1, working_seed);
    working_seed += 1;
    gsl_rng_set(r2, working_seed);
    working_seed += 1;
    gsl_rng_set(r3, working_seed);
    working_seed += 1;
    gsl_rng_set(r4, working_seed);
    working_seed += 1;

    series_model model;
    model.steps_count = series_size;
    model.t_step = 1.0;
    model.amplitude = 10.0 * gsl_rng_uniform(r1);
    model.frequency = 0.01 + 1.99 * gsl_rng_uniform(r2);
    model.phase = 2 * M_PI * gsl_rng_uniform(r3);
    model.SNR = 0.1 + 99.9 * gsl_rng_uniform(r4);
    model.lin_trend_a = 0.0;
    model.lin_trend_b = 0.0;

    return model;
}

series_model get_random_model_2(int series_size, double SNR)
{
    gsl_rng *r1, *r2, *r3;
    gsl_rng_env_setup();
    r1 = gsl_rng_alloc(gsl_rng_default);
    r2 = gsl_rng_alloc(gsl_rng_default);
    r3 = gsl_rng_alloc(gsl_rng_default);
    gsl_rng_set(r1, working_seed);
    working_seed += 1;
    gsl_rng_set(r2, working_seed);
    working_seed += 1;
    gsl_rng_set(r3, working_seed);
    working_seed += 1;

    series_model model;
    model.steps_count = series_size;
    model.t_step = 1.0;
    model.amplitude = 10.0 * gsl_rng_uniform(r1);
    model.frequency = 0.1 + 1.9 * gsl_rng_uniform(r2);
    model.phase = 2 * M_PI * gsl_rng_uniform(r3);
    model.SNR = SNR;
    model.lin_trend_a = 0.0;
    model.lin_trend_b = 0.0;

    return model;
}

double *get_correlogram_full(int series_size, double *times, double *series)
{
    //double *centered_series = exclude_trend(series_size, times, series, false);
    double *centered_series = series;
    double mean = gsl_stats_mean(centered_series, 1, series_size);
    //printf("%lf\n", mean);
    for (int i = 0; i < series_size; i++)
        centered_series[i] -= mean;

    int complex_series_size, periodogram_size;
    double *complex_series, *frequencies, *periodogram;
    double time_step = times[1] - times[0];
    get_periodogram(series_size, time_step, centered_series, &complex_series_size,
                    &complex_series, &periodogram_size, &frequencies, &periodogram);
    double *correlogram = get_correlogram(series_size, complex_series_size, complex_series);
    return correlogram;
}

double get_mean_correlogram_value(int series_size, double *correlogram)
{
    double *array = malloc(sizeof(double) * series_size);
    for (int i = 0; i < series_size; i++)
        array[i] = fabs(correlogram[i]);

    double mean_value = gsl_stats_mean(array, 1, series_size);
    free(array);
    return mean_value;
}

void get_one_generation_1(int series_size, double *mean_value, double *amplitude, double *SNR, double *corr0)
{
    double *times, *series;
    series_model model = get_random_model_1(series_size);
    get_series(&model, &times, &series);

    double *correlogram = get_correlogram_full(series_size, times, series);
    *mean_value = get_mean_correlogram_value(series_size, correlogram);
    *amplitude = model.amplitude;
    *SNR = model.SNR;
    *corr0 = correlogram[0];
}

void get_one_generation_2(int series_size, double SNR, double *mean_value, double *amplitude, double *corr0)
{
    double *times, *series;
    series_model model = get_random_model_2(series_size, SNR);
    get_series(&model, &times, &series);

    double *correlogram = get_correlogram_full(series_size, times, series);
    *mean_value = get_mean_correlogram_value(series_size, correlogram);
    *amplitude = model.amplitude;
    *corr0 = correlogram[0];
}

int main(int argc, char const *argv[])
{
    int series_size = 1000;
    int generation_count = 100; // 2500
    double mean_value, amplitude, SNR, corr0;
    double amplitude2, tmp_div, tmp_num;

    double *SNR_true_array = malloc(sizeof(double) * generation_count);
    double *SNR_estimate_array = malloc(sizeof(double) * generation_count);

    double *mean_value_array = malloc(sizeof(double) * generation_count);
    double *amplitude2_array = malloc(sizeof(double) * generation_count);
    for (int i = 0; i < generation_count; i++)
    {
        //get_one_generation_1(series_size, &mean_value, &amplitude, &SNR, &corr0);
        SNR = 0.001 + i * 0.001;
        get_one_generation_2(series_size, SNR, &mean_value, &amplitude, &corr0);
        
        mean_value_array[i] = mean_value;
        amplitude2_array[i] = pow(amplitude, 2);
        
        amplitude2 = mean_value / 0.1591;
        tmp_num = amplitude2;
        tmp_div = (2.0 * series_size / (series_size - 1.0) * corr0 - amplitude2);
        SNR_estimate_array[i] = tmp_num / tmp_div; 
        
        /*
        printf("a^2/2: %lf \n", amplitude2 / 2);
        printf("sigma^2: %lf\n", series_size / (series_size - 1.0) * corr0);
        printf("SRN est: %lf \nSNR true: %lf \n", SNR_estimate_array[i], SNR);
        printf("-------------------------\n");
        */
        SNR_true_array[i] = SNR;
        
    }
    //print_series2_in_file("num_exp1.dat", generation_count, amplitude2_array, mean_value_array);
    print_series2_in_file("num_exp5.dat", generation_count, SNR_true_array, SNR_estimate_array);

    double *SNR_eps_array = malloc(sizeof(double) * generation_count);
    for (int i = 0; i < generation_count; i++)
        SNR_eps_array[i] = 100 * fabs(SNR_true_array[i] - SNR_estimate_array[i]) / SNR_true_array[i];
    //print_series2_in_file("num_exp44.dat", generation_count, SNR_true_array, SNR_eps_array);
    //double correlation = gsl_stats_correlation(SNR_true_array, 1, SNR_estimate_array, 1, series_size);
    //printf("correlation %lf \n", correlation);

    double c0, c1, cov00, cov01, cov11, sum_sq;
    //gsl_fit_linear(amplitude2_array, 1, mean_value_array, 1, generation_count, &c0, &c1, &cov00, &cov01, &cov11, &sum_sq);
    gsl_fit_linear(SNR_true_array, 1, SNR_estimate_array, 1, generation_count, &c0, &c1, &cov00, &cov01, &cov11, &sum_sq);
    printf("a: %f +- %f \n", c0, sqrt(cov00));
    printf("b: %f +- %f \n", c1, sqrt(cov11));
    
    return 0;
}
