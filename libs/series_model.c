#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "series_model.h"

gsl_rng *_r;

double get_series_value(double time, series_model *model)
{
    double linear_trend_term = model->lin_trend_a + model->lin_trend_b * time;
    double harmonic_term = model->amplitude * sin(2 * M_PI * model->frequency * time + model->phase);
    double noise_term = sqrt(pow(model->amplitude, 2) / (2 * model->SNR)) * gsl_ran_gaussian(_r, 1);
    return linear_trend_term + harmonic_term + noise_term;
}

void get_series(series_model *model, double **times, double **series)
{
    const gsl_rng_type *T;
    gsl_rng_env_setup();
    T = gsl_rng_default;
    _r = gsl_rng_alloc(T);

    *times  = malloc(sizeof(double) * model->steps_count);
    *series = malloc(sizeof(double) * model->steps_count);

    for (int i = 0; i < model->steps_count; i++)
    {
        double time = i * model->t_step;
        (*times)[i] = time;
        (*series)[i] = get_series_value(time, model);
    }
    gsl_rng_free(_r);
}
