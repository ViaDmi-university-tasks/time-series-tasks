#ifndef SCATS_H
#define SCATS_H
#include <stdbool.h>

double *exclude_trend(int series_size, double *times, double *series, bool is_printed);

int get_radix2_size(int size);

double *_transform_to_complex(int series_size, double *series, int complex_series_size);

void get_periodogram(int series_size, double time_step, double *series,
                     int *complex_series_size, double **complex_series,
                     int *periodogram_size, double **frequencies, double **periodogram);

double *get_correlogram(int series_size, int complex_series_size, double *complex_series);

double _tukey_weight(int m, int size, double a);

double *get_weighted_correlogram(int weighted_correlogram_size, double a, double *correlogram);

double *get_smoothed_periodogram(double a, int smoothed_periodogram_size,
                                 int complex_series_size, double *correlogram, int periodogram_size);

#endif /* SCATS_H */